В проекте используется подключение к mongodb.
Для работы требуется docker. Создаем папку mongo с файлом docker-compose.yml: 
```
version: '3'
services:
  mongo:
    image: mongo:4.4.4
    container_name: mongo
    restart: always
    environment:
      - MONGO_INITDB_ROOT_USERNAME=admin
      - MONGO_INITDB_ROOT_PASSWORD=admin
    ports:
      - 27017:27017
    volumes:
      - ./mongo-data-4.4:/data/db
    command: --wiredTigerCacheSizeGB 1.5
```

Запускаем mongodb из папки mongo с помощью команды.
```
docker-compose up -d
```
Для остановки docker-контейнера: 
```
docker stop mongo
```
Для повторного запуска
```
docker start mongo
```