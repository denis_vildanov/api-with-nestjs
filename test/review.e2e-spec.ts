import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { CreateReviewDto } from '../src/review/dto/create-review.dto';
import { disconnect, Types } from 'mongoose';
import { REVIEW_NOT_FOUND } from '../src/review/review.constants';

const  productId = new Types.ObjectId().toHexString();

const testDto: CreateReviewDto =  {
  name: 'Test',
  title: 'Title',
  description: 'Test description',
  rating: 5,
  productId
};

describe('AppController (e2e)', () => {
  let app: INestApplication;
  let reviewId: string;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/review/create (POST)', async (done) => {
    return request(app.getHttpServer())
      .post('/review/create')
      .send(testDto)
      .expect(201)
      .expect(({ body }: request.Response) => {
        reviewId = body._id;

        expect(reviewId).toBeDefined();
        done();
      });
  });

  it('/byProduct/:productId (GET) - success', async (done) => {
    return request(app.getHttpServer())
      .get(`/review/byProduct/${productId}`)
      .expect(200)
      .then((res: request.Response) => {
        expect(res.body.length).toBe(1);
        done();
      });
  });

  it('/byProduct/:productId (GET) - fail', async (done) => {
    return request(app.getHttpServer())
      .get(`/review/byProduct/${new Types.ObjectId().toHexString()}`)
      .expect(200)
      .then((res: request.Response) => {
        expect(res.body.length).toBe(0);
        done();
      });
  });

  it('/review/:id (DELETE) - success', () => {
    return request(app.getHttpServer())
      .delete(`/review/${reviewId}`)
      .expect(200)
  });

  it('/review/:id (DELETE) - not found', () => {
    return request(app.getHttpServer())
      .delete(`/review/${new Types.ObjectId().toHexString()}`)
      .expect(
        404,
        {
          statusCode: 404,
          message: REVIEW_NOT_FOUND,
        }
      )
  });

  afterAll(() => {
    disconnect();
  })
});
